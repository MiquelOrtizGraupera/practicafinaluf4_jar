package ShopDAOS.DAO.comanda;

import java.io.Serializable;
import java.sql.Date;

public class Comanda implements Serializable {

    private int id_comanda;
    private int id_producte;
    private String data_comanda;
    private int quantitat;
    private int id_prov;
    private String data_tramesa;
    private int total;

    public Comanda(int id_comanda, int id_producte, String data_comanda, int quantitat, int id_prov, String data_tramesa, int total) {
        this.id_comanda = id_comanda;
        this.id_producte = id_producte;
        this.data_comanda = data_comanda;
        this.quantitat = quantitat;
        this.id_prov = id_prov;
        this.data_tramesa = data_tramesa;
        this.total = total;
    }

    public Comanda(){}

    @Override
    public String toString() {
        return "Comanda{" +
                "id_comanda=" + id_comanda +
                ", id_producte=" + id_producte +
                ", data_comanda=" + data_comanda +
                ", quantitat=" + quantitat +
                ", id_prov=" + id_prov +
                ", data_tramesa=" + data_tramesa +
                ", total=" + total +
                '}';
    }

    public int getId_comanda() {
        return id_comanda;
    }

    public void setId_comanda(int id_comanda) {
        this.id_comanda = id_comanda;
    }

    public int getId_producte() {
        return id_producte;
    }

    public void setId_producte(int id_producte) {
        this.id_producte = id_producte;
    }

    public String getData_comanda() {
        return data_comanda;
    }

    public void setData_comanda(String data_comanda) {
        this.data_comanda = data_comanda;
    }

    public int getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(int quantitat) {
        this.quantitat = quantitat;
    }

    public int getId_prov() {
        return id_prov;
    }

    public void setId_prov(int id_prov) {
        this.id_prov = id_prov;
    }

    public String getData_tramesa() {
        return data_tramesa;
    }

    public void setData_tramesa(String data_tramesa) {
        this.data_tramesa = data_tramesa;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
