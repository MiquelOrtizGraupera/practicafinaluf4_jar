package ShopDAOS.DAO.comanda;

import ShopDAOS.DAOFactory.MongoDBDAOFactory;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class CompandaImplMongoDB implements ComandaDAO{
MongoDatabase mongoDatabase;
String coleccio = "comanda";

public CompandaImplMongoDB(){
    mongoDatabase = MongoDBDAOFactory.getDataBase();
}

    @Override
    public boolean insertar(Comanda comanda) {
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDatabase = mongoDatabase.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Comanda> mongoCollection = mongoDatabase.getCollection(coleccio,Comanda.class);

        for (Comanda c : comandes){
            mongoCollection.insertOne(c);
        }
        System.out.println("Insertat a MongoDB!");

    return comandes.size();
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        return null;
    }
}
