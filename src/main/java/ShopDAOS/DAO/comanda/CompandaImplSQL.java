package ShopDAOS.DAO.comanda;

import ShopDAOS.DAO.empleat.Empleat;
import ShopDAOS.DAOFactory.SQLdbDAOFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompandaImplSQL implements ComandaDAO{
    Connection conexion;

    public CompandaImplSQL(){
        conexion = SQLdbDAOFactory.crearConexion();
    }

    @Override
    public boolean insertar(Comanda comanda) {
        boolean valor = false;
        String querySQL = "INSERT INTO comanda VALUES (?,?,?,?,?,?,?)";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1,comanda.getId_comanda());
            statement.setInt(2,comanda.getId_producte());
            statement.setString(3, String.valueOf(java.sql.Date.valueOf(comanda.getData_comanda())));
            statement.setInt(4,comanda.getQuantitat());
            statement.setInt(5,comanda.getId_prov());
            statement.setString(6, String.valueOf(java.sql.Date.valueOf(comanda.getData_tramesa())));
            statement.setInt(7,comanda.getTotal());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Comanda num--> %d insertada",comanda.getId_comanda());
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return valor;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        String querySQL = "INSERT INTO comanda VALUES (?,?,?,?,?,?,?)";
        PreparedStatement statement;
        int filas = 0;
        try{
            statement = conexion.prepareStatement(querySQL);
            for (int i = 0; i < comandes.size() ; i++) {
                statement.setInt(1,comandes.get(i).getId_comanda());
                statement.setInt(2, comandes.get(i).getId_producte());
                statement.setDate(3,java.sql.Date.valueOf(comandes.get(i).getData_comanda()));
                statement.setInt(4,comandes.get(i).getQuantitat());
                statement.setInt(5,comandes.get(i).getId_prov());
                statement.setDate(6,java.sql.Date.valueOf(comandes.get(i).getData_tramesa()));
                statement.setInt(7,comandes.get(i).getTotal());
                statement.executeUpdate();
                filas++;
            }
            if(filas > 0){
                System.out.println("Comandes insertades: "+filas);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return filas;
    }

    @Override
    public boolean eliminar(int comandaId) {
        boolean valor = false;
        String querySQL = "DELETE FROM comanda WHERE id_comanda = ?";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1,comandaId);
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Comanda %d eliminada%n", comandaId);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean eliminarConjunt(){
        //NO CAL IMPLEMENTAR
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        Boolean valor = false;
        String querySQL = "UPDATE SET id_producte = ?, data_comanda = ?, quantitat = ?, id_prov = ?," +
                " data_tramesa = ?, total = ? WHERE id_comanda = ?";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(7, comanda.getId_comanda());
            statement.setInt(1, comanda.getId_producte());
            statement.setString(2, comanda.getData_comanda());
            statement.setInt(3, comanda.getQuantitat());
            statement.setInt(4,comanda.getId_prov());
            statement.setString(5,comanda.getData_tramesa());
            statement.setInt(6,comanda.getTotal());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Comanda %d modificada amb exit%n", comanda.getId_comanda());
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public Comanda consultar(int comandaId) {
        String querySQL = "SELECT * FROM comanda WHERE id_comanda = ?";
        PreparedStatement statement;
        Comanda comanda = new Comanda();

        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1, comandaId);
            ResultSet resultat = statement.executeQuery();
            if(resultat.next()){
                comanda.setId_comanda(resultat.getInt("id_comanda"));
                comanda.setId_producte(resultat.getInt("id_producte"));
                comanda.setData_comanda(resultat.getString("data_comanda"));
                comanda.setId_prov(resultat.getInt("id_prov"));
                comanda.setData_tramesa(resultat.getString("data_tramesa"));
                comanda.setTotal(resultat.getInt("total"));
            }else{
                System.out.printf("La comanda %d no existeix%n", comandaId);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return comanda;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        String querySQL = "SELECT * from comanda";
        PreparedStatement statement;
        List<Comanda> llista = new ArrayList<>();

        try{
            statement = conexion.prepareStatement(querySQL);
            ResultSet resultat = statement.executeQuery();
            while(resultat.next()){
                Comanda comanda = new Comanda();

                comanda.setId_comanda(resultat.getInt("id_comanda"));
                comanda.setId_producte(resultat.getInt("id_producte"));
                comanda.setData_comanda(resultat.getString("data_comanda"));
                comanda.setId_prov(resultat.getInt("id_prov"));
                comanda.setData_tramesa(resultat.getString("data_tramesa"));
                comanda.setTotal(resultat.getInt("total"));

                llista.add(comanda);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return llista;
    }
}
