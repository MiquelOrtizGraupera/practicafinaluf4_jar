package ShopDAOS.DAO.empleat;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;


public class Empleat implements Serializable {

    private int id;
    private String cognom;
    private String ofici;
    private int capId;
    private String dataAlta;
    private int salari;
    private double comissio;
    private int depNo;

    public Empleat(){}

    public Empleat(int id, String cognom, String ofici, int capId, String dataAlta, int salari, double comissio, int depNo) {
        this.id = id;
        this.cognom = cognom;
        this.ofici = ofici;
        this.capId = capId;
        this.dataAlta = dataAlta;
        this.salari = salari;
        this.comissio = comissio;
        this.depNo = depNo;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "emplatID=" + id +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", capId=" + capId +
                ", dataAlta='" + dataAlta + '\'' +
                ", salari=" + salari +
                ", comissio=" + comissio +
                ", depNo=" + depNo +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public int getCapId() {
        return capId;
    }

    public void setCapId(int capId) {
        this.capId = capId;
    }

    public String getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(String dataAlta) {
        this.dataAlta = dataAlta;
    }

    public int getSalari() {
        return salari;
    }

    public void setSalari(int salari) {
        this.salari = salari;
    }

    public double getComissio() {
        return comissio;
    }

    public void setComissio(double comissio) {
        this.comissio = comissio;
    }

    public int getDepNo() {
        return depNo;
    }

    public void setDepNo(int depNo) {
        this.depNo = depNo;
    }

}





