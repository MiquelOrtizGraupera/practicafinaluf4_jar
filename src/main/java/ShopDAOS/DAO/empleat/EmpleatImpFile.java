package ShopDAOS.DAO.empleat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class EmpleatImpFile implements EmpleatDAO {
    File fileName;
    public EmpleatImpFile() {
        fileName = new File("empleats.json");
    }

    @Override
    public boolean insertar(Empleat emp) {
        System.out.println("No implementat");
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps){
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(emps);

        Path dir = Path.of("./src/shopFitxersJSON");
        if(!Files.exists(dir)){
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try(PrintWriter escribir = new PrintWriter(new FileWriter(file))){
                escribir.write(json);
            }catch (Exception e){
                e.printStackTrace();
            }
            System.out.println("Fitxer creat amb exit");
        }else{
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter escribir = new PrintWriter(new FileWriter(file))) {
                escribir.write(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Fitxer creat amb exit");
        }
        return emps.size();
    }

    @Override
    public boolean eliminar(int empId) {
        System.out.println("No implementat");
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        System.out.println("No implementat");
        return true;
    }

    @Override
    public boolean modificar(Empleat emp) {
        System.out.println("No implementat");
        return false;
    }

    @Override
    public Empleat consultar(int empId) {
        System.out.println("No implementat");
        return null;
    }

    @Override
    public List<Empleat> consultarLlista() {
        List<Empleat> llista = new ArrayList<>();
        try{
            Reader reader = Files.newBufferedReader(Paths.get("./src/shopFitxersJSON/"+fileName));

            if(Files.exists(Paths.get("./src/shopFitxersJSON/"+fileName))){
                System.out.println("El fitxer existeix...ens disposem a passar la info a una llista");
            }else{
                System.out.println("On està el fitxer??");
            }

            llista = new Gson().fromJson(reader,new TypeToken<List<Empleat>>() {}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return llista;
    }
}
