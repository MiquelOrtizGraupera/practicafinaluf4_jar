package ShopDAOS.DAO.empleat;


import ShopDAOS.DAOFactory.MongoDBDAOFactory;
import com.mongodb.MongoClient;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


public class EmpleatImpMongoDB implements EmpleatDAO {
    MongoDatabase mongoDB;
    String coleccio = "empleats";

    public EmpleatImpMongoDB() {
        mongoDB = MongoDBDAOFactory.getDataBase();
    }


    @Override
    public boolean insertar(Empleat emps) {return false;}

    @Override
    public int insertarLlista(List<Empleat> emps) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Empleat> mongoCollection = mongoDB.getCollection(coleccio,Empleat.class);
        mongoCollection.insertMany(emps);
        System.out.println("Insertat a MongoDB!!");
        return emps.size();
    }

    @Override
    public boolean eliminar(int empId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        return false;
    }

    @Override
    public Empleat consultar(int empId) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Empleat> mongoCollection = mongoDB.getCollection(coleccio,Empleat.class);

        FindIterable<Empleat> emp = mongoCollection.find();

        Empleat empleat = new Empleat();
        for (Empleat e : emp){
            if(e.getId() == empId){
                System.out.println("L'empleat existeix");
                empleat = e;
                System.out.println(empleat);
            }
        }

        return empleat;
    }

    @Override
    public List<Empleat> consultarLlista() {

        return null;
    }
}
