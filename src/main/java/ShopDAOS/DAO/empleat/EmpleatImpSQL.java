package ShopDAOS.DAO.empleat;

import ShopDAOS.DAOFactory.SQLdbDAOFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class EmpleatImpSQL implements EmpleatDAO {
    Connection conexion;

//    {
//        try{
//            conexion = ConexioSQL.conexioSQL();
//        }catch (SQLException ex){
//            ex.printStackTrace();
//        }
//    }

    public EmpleatImpSQL() {
        conexion = SQLdbDAOFactory.crearConexion();
    }

    @Override
    public boolean insertar(Empleat emps) {
        boolean valor = false;
        String querySQL = "INSERT INTO empleados VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1,emps.getId());
            statement.setString(2, emps.getCognom());
            statement.setString(3, emps.getOfici());
            statement.setInt(4,emps.getCapId());
            statement.setDate(5,java.sql.Date.valueOf(String.valueOf(emps.getDataAlta())));
            statement.setInt(6,emps.getSalari());
            statement.setDouble(7,emps.getComissio());
            statement.setInt(8,emps.getDepNo());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Empleado %s insertado %n", emps.getCognom());
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {
        String querySQL = "INSERT INTO empleat VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement statement;
        int filas = 0;
        try{
            statement = conexion.prepareStatement(querySQL);
            for (int i = 0; i < emps.size() ; i++) {
                statement.setInt(1, emps.get(i).getId());
                statement.setString(2, emps.get(i).getCognom());
                statement.setString(3, emps.get(i).getOfici());
                statement.setInt(4,emps.get(i).getCapId());
                statement.setDate(5,java.sql.Date.valueOf(String.valueOf(emps.get(i).getDataAlta())));
                statement.setInt(6,emps.get(i).getSalari());
                statement.setDouble(7,emps.get(i).getComissio());
                statement.setInt(8,emps.get(i).getDepNo());
                statement.executeUpdate();
                filas++;
            }
            if(filas > 0){
                System.out.println("Empleados insertados: "+filas);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return filas;
    }

    @Override
    public boolean eliminar(int empId) {
        boolean valor = false;
        String querySQL = "DELETE FROM empleat WHERE id_empleat = ?";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1, empId);
           int filas = statement.executeUpdate();
           if(filas > 0){
               valor = true;
               System.out.printf("Empleado %d eliminado %n",empId);
           }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean eliminarConjunt() {
        // NO CAL IMPLEMENTAR-HO
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        boolean valor = false;
        String querySQL = " UPDATE SET cognom = ?, ofici = ?, cap_id = ?, data_alta = ?, salari = ?, comisio = ?, " +
                "dept_no = ? WHERE id_empleat = ?";
        PreparedStatement statement;
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(8,emp.getId());
            statement.setString(1,emp.getCognom());
            statement.setString(2, emp.getOfici());
            statement.setInt(3,emp.getCapId());
            statement.setDate(4,java.sql.Date.valueOf(String.valueOf(emp.getDataAlta())));
            statement.setInt(5,emp.getSalari());
            statement.setDouble(6,emp.getComissio());
            statement.setInt(7,emp.getDepNo());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Empleat amb cognom %s modificat amb exit%n",emp.getCognom());
            }
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return valor;
    }

    @Override
    public Empleat consultar(int empId) {
        String querySQL = "SELECT * FROM empleat WHERE id_empleat = ?";
        PreparedStatement statement;
        Empleat emp = new Empleat();
        try{
            statement = conexion.prepareStatement(querySQL);
            statement.setInt(1,empId);
            ResultSet resultat = statement.executeQuery();
            if(resultat.next()){
                emp.setId(resultat.getInt("id_empleat"));
                emp.setCognom(resultat.getString("cognom"));
                emp.setOfici(resultat.getString("ofici"));
                emp.setCapId(resultat.getInt("cap_id"));
                emp.setDataAlta(resultat.getString("data_alta"));
                emp.setSalari(resultat.getInt("salari"));
                emp.setComissio(resultat.getDouble("comisio"));
                emp.setDepNo(resultat.getInt("dept_no"));
            }else{
                System.out.printf("L'empleat %d no existeix %n",empId);
            }
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return emp;
    }

    @Override
    public List<Empleat> consultarLlista(){
        String querySQL = "SELECT * FROM empleat";
        EmpleatImpSQL empSQL = new EmpleatImpSQL();
        List<Empleat> llista = new ArrayList<>();
        try{
            Statement statement = empSQL.conexion.createStatement();
            ResultSet resultat = statement.executeQuery(querySQL);

            while(resultat.next()){
                Empleat emp = new Empleat();

                emp.setId(resultat.getInt("id_empleat"));
                emp.setCognom(resultat.getString("cognom"));
                emp.setOfici(resultat.getString("ofici"));
                emp.setCapId(resultat.getInt("cap_id"));
                emp.setDataAlta(resultat.getString("data_alta"));
                emp.setSalari(resultat.getInt("salari"));
                emp.setComissio(resultat.getDouble("comissio"));
                emp.setDepNo(resultat.getInt("dept_no"));

                llista.add(emp);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return llista;
    }
}
