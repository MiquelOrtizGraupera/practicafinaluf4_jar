package ShopDAOS.DAO.producte;

import ShopDAOS.DAOFactory.MongoDBDAOFactory;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ProducteImplMongoDB implements ProducteDAO{
MongoDatabase mongoDatabase;
String collecio = "producte";

public  ProducteImplMongoDB(){
    mongoDatabase = MongoDBDAOFactory.getDataBase();
}

    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDatabase = mongoDatabase.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Producte> mongoCollection = mongoDatabase.getCollection(collecio,Producte.class);

        for (Producte p: productes){
            mongoCollection.insertOne(p);
        }
        System.out.println("Insertat a MongoDB");
    return productes.size();
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        return null;
    }

    @Override
    public List<Producte> consultarLlista() {
        return null;
    }
}
