package ShopDAOS.DAO.producte;

import ShopDAOS.DAOFactory.SQLdbDAOFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProducteImplSQL implements ProducteDAO{
    Connection conexio;

    public ProducteImplSQL(){conexio = SQLdbDAOFactory.crearConexion();}

    @Override
    public boolean insertar(Producte product) {
        boolean valor = false;
        String querySQL = "INSERT INTO producte VALUES (?,?,?,?,?)";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1, product.getId_producte());
            statement.setString(2,product.getDescripcio());
            statement.setInt(3,product.getStockactual());
            statement.setInt(4, product.getStockminim());
            statement.setDouble(5,product.getPreu());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Producte %d insertat amb exit%n", product.getId_producte());
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        String querySQL = "INSERT INTO producte VALUES(?,?,?,?,?)";
        PreparedStatement statement;
        int filas = 0;
        try{
            statement = conexio.prepareStatement(querySQL);
            for (int i = 0; i < productes.size() ; i++) {
                statement.setInt(1, productes.get(i).getId_producte());
                statement.setString(2,productes.get(i).getDescripcio());
                statement.setInt(3, productes.get(i).getStockactual());
                statement.setInt(4,productes.get(i).getStockminim());
                statement.setDouble(5,productes.get(i).getPreu());
                statement.executeUpdate();
                filas++;
            }
            if(filas > 0){
                System.out.println("Empleados insertados: "+filas);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return filas;
    }

    @Override
    public boolean eliminar(int productId) {
        boolean valor = false;
        String querySQL = "DELETE FROM producte WHERE id_producte = ?";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1, productId);
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Producte %d eliminat amb exit%n",productId);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        boolean valor = false;
        String querySQL = "UPDATE SET stockactual = ? WHERE id_producte = ?";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(2,product.getId_producte());
            statement.setInt(1, product.getStockactual());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Stock del prouducte %d modificat amb exit%n",product.getId_producte());
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public Producte consultar(int productID) {
        String querySQL = "SELECT * FROM producte WHERE id_producte = ?";
        PreparedStatement statement;
        Producte prod = new Producte();
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1, productID);
            ResultSet resultat = statement.executeQuery();
            if(resultat.next()){
                prod.setId_producte(resultat.getInt("id_producte"));
                prod.setDescripcio(resultat.getString("descripcio"));
                prod.setStockactual(resultat.getInt("stockactual"));
                prod.setStockminim(resultat.getInt("stockminim"));
                prod.setPreu(resultat.getDouble("preu"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Producte> consultarLlista() {
        String querySQL = "SELECT * FROM producte";
        PreparedStatement statement;
        List<Producte> llista = new ArrayList<>();


        try{
            statement = conexio.prepareStatement(querySQL);
            ResultSet resultat = statement.executeQuery();
            while(resultat.next()){
                Producte prod = new Producte();

                prod.setId_producte(resultat.getInt("id_producte"));
                prod.setDescripcio(resultat.getString("descripcio"));
                prod.setStockactual(resultat.getInt("stockactual"));
                prod.setStockminim(resultat.getInt("stockminim"));
                prod.setPreu(resultat.getDouble("preu"));

                llista.add(prod);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return llista;
    }
}
