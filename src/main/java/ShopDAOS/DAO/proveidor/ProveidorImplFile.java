package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAO.empleat.Empleat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImplFile implements ProveidorDAO {
    File fileName;

    public ProveidorImplFile(){
        fileName = new File("proveidors.json");
    }

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(provs);

        Path dir = Path.of("./src/shopFitxersJSON");
        if(!Files.exists(dir)){
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try(PrintWriter escribir = new PrintWriter(new FileWriter(file))){
                escribir.write(json);
            }catch (Exception e){
                e.printStackTrace();
            }
            System.out.println("Fitxer creat amb exit");
        }else{
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter escribir = new PrintWriter(new FileWriter(file))) {
                escribir.write(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Fitxer creat amb exit");
        }
        return provs.size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        List<Proveidor> llista = new ArrayList<>();
        try{
            Reader reader = Files.newBufferedReader(Paths.get("./src/shopFitxersJSON/"+fileName));

            if(Files.exists(Paths.get("./src/shopFitxersJSON/"+fileName))){
                System.out.println("El fitxer existeix...ens disposem a passar la info a una llista");
            }else{
                System.out.println("On està el fitxer??");
            }
            llista = new Gson().fromJson(reader,new TypeToken<List<Proveidor>>() {}.getType());
        }catch (IOException e){
            e.printStackTrace();
        }
        return llista;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
