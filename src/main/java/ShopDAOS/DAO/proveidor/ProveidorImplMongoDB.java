package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAO.comanda.Comanda;
import ShopDAOS.DAO.producte.Producte;
import ShopDAOS.DAOFactory.MongoDBDAOFactory;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ProveidorImplMongoDB implements ProveidorDAO{
MongoDatabase mongoDatabase;
String coleccio = "proveidor";

public ProveidorImplMongoDB(){
    mongoDatabase = MongoDBDAOFactory.getDataBase();
}

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDatabase = mongoDatabase.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Proveidor> mongoCollection = mongoDatabase.getCollection(coleccio, Proveidor.class);

        for (Proveidor p : provs){
            mongoCollection.insertOne(p);
        }
        System.out.println("Insertat a MongoDB");
        return provs.size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        return null;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
