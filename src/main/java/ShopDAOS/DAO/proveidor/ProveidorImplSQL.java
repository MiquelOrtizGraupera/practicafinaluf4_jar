package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAOFactory.SQLdbDAOFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImplSQL implements ProveidorDAO{
    Connection conexio;

    public ProveidorImplSQL(){conexio = SQLdbDAOFactory.crearConexion();}

    @Override
    public boolean insertar(Proveidor prov) {
        boolean valor = false;
        String querySQL = "INSERT INTO prov VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1, prov.getId_prov());
            statement.setString(2,prov.getNom());
            statement.setString(3, prov.getAdreca());
            statement.setString(4,prov.getCiutat());
            statement.setString(5,prov.getEstat());
            statement.setString(6,prov.getCodi_postal());
            statement.setInt(7,prov.getArea());
            statement.setString(8,prov.getTelefon());
            statement.setInt(9,prov.getId_producte());
            statement.setInt(10,prov.getQuantitat());
            statement.setDouble(11,prov.getLimit_credit());
            statement.setString(12,prov.getObservacions());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Proveidor %d insertat amb exit%n",prov.getId_prov());
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        String querySQL = "INSERT INTO prov VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement statement;
        int filas = 0;
        try{
         statement = conexio.prepareStatement(querySQL);
            for (int i = 0; i < provs.size() ; i++) {
                statement.setInt(1, provs.get(i).getId_prov());
                statement.setString(2,provs.get(i).getNom());
                statement.setString(3, provs.get(i).getAdreca());
                statement.setString(4,provs.get(i).getCiutat());
                statement.setString(5,provs.get(i).getEstat());
                statement.setString(6,provs.get(i).getCodi_postal());
                statement.setInt(7,provs.get(i).getArea());
                statement.setString(8,provs.get(i).getTelefon());
                statement.setInt(9,provs.get(i).getId_producte());
                statement.setInt(10,provs.get(i).getQuantitat());
                statement.setDouble(11,provs.get(i).getLimit_credit());
                statement.setString(12,provs.get(i).getObservacions());
                statement.executeUpdate();
                filas++;
            }
            if(filas > 0){
                System.out.println("Proveidors insertats: "+ filas);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return filas;
    }

    @Override
    public boolean eliminar(int idProv) {
        boolean valor = false;
        String querySQL = "DELETE FROM prov WHERE id_prov = ?";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1,idProv);
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Proveidor amb id: %d  eliminat amb exit%n",idProv);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        boolean valor = false;
        String querySQL = "UPDATE SET quantitat = ? WHERE id_prov = ?";
        PreparedStatement statement;
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(2, prov.getId_prov());
            statement.setInt(1,prov.getQuantitat());
            int filas = statement.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Quantitat modificada de proveidor %d%n", prov.getId_prov());
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }

    @Override
    public Proveidor consultar(int idProv) {
        String querySQL = "SELECT * FROM prov WHERE id_prov = ?";
        PreparedStatement statement;
        Proveidor prov = new Proveidor();
        try{
            statement = conexio.prepareStatement(querySQL);
            statement.setInt(1, idProv);
            ResultSet resultat = statement.executeQuery();
            if(resultat.next()){
                prov.setId_prov(resultat.getInt("id_prov"));
                prov.setNom(resultat.getString("nom"));
                prov.setAdreca(resultat.getString("adreca"));
                prov.setCiutat(resultat.getString("ciutat"));
                prov.setEstat(resultat.getString("estat"));
                prov.setCodi_postal(resultat.getString("codi_postal"));
                prov.setArea(resultat.getInt("area"));
                prov.setTelefon(resultat.getString("telefon"));
                prov.setId_producte(resultat.getInt("id_producte"));
                prov.setQuantitat(resultat.getInt("quantitat"));
                prov.setLimit_credit(resultat.getDouble("limit_credit"));
                prov.setObservacions(resultat.getString("observacions"));
            }else{
                System.out.printf("El proveidor %d no existeix%n",prov.getId_prov());
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return prov;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        String querySQL = "SELECT * FROM prov";
        PreparedStatement statement;
        List<Proveidor> llista = new ArrayList<>();


        try{
            statement = conexio.prepareStatement(querySQL);
            ResultSet resultat = statement.executeQuery();
            while(resultat.next()){
                Proveidor prov = new Proveidor();

                prov.setId_prov(resultat.getInt("id_prov"));
                prov.setNom(resultat.getString("nom"));
                prov.setAdreca(resultat.getString("adreca"));
                prov.setCiutat(resultat.getString("ciutat"));
                prov.setEstat(resultat.getString("estat"));
                prov.setCodi_postal(resultat.getString("codi_postal"));
                prov.setArea(resultat.getInt("area"));
                prov.setTelefon(resultat.getString("telefon"));
                prov.setId_producte(resultat.getInt("id_producte"));
                prov.setQuantitat(resultat.getInt("quantitat"));
                prov.setLimit_credit(resultat.getDouble("limit_credit"));
                prov.setObservacions(resultat.getString("observacions"));

                llista.add(prov);
            }
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return llista;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
