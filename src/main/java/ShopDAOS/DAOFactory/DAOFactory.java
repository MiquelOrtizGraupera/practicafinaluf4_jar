package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.empleat.EmpleatDAO;
import ShopDAOS.DAO.producte.ProducteDAO;
import ShopDAOS.DAO.proveidor.ProveidorDAO;

public abstract class DAOFactory {
    //Bases de datos soportadas
    public static final int POSTGRESQL = 1;
    public static final int MONGODB = 2;

    public abstract ComandaDAO getComandaDAO();
    public abstract EmpleatDAO getEmpleatDAO();
    public abstract ProducteDAO getProducteDAO();
    public abstract ProveidorDAO getProveidorDAO();

    public static DAOFactory getDAOFactory(int db){
        switch (db){
            case POSTGRESQL:
                return new SQLdbDAOFactory();
            case MONGODB:
                return new MongoDBDAOFactory();
            default: return null;
        }
    }
}
