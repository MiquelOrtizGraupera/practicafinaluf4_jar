package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.comanda.CompandaImplMongoDB;
import ShopDAOS.DAO.empleat.EmpleatDAO;
import ShopDAOS.DAO.empleat.EmpleatImpMongoDB;
import ShopDAOS.DAO.producte.ProducteDAO;
import ShopDAOS.DAO.producte.ProducteImplMongoDB;
import ShopDAOS.DAO.proveidor.ProveidorDAO;
import ShopDAOS.DAO.proveidor.ProveidorImplMongoDB;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;



public class MongoDBDAOFactory extends DAOFactory {
    static String USER = "OrtizMiquel";
    static String PASS = "ortizmiquel";
    static String HOST = "cluster0.gniew.mongodb.net";

    private static MongoClient mongoClient;

    public static MongoDatabase getDataBase() {
        mongoClient = MongoClients.create("mongodb+srv://"+USER+":"+PASS+"@"+HOST+"/retryWrites=true&w=majority");

        return mongoClient.getDatabase("Shop");
    }

    public static void closeConnection() {
        mongoClient.close();
    }


    @Override
    public ComandaDAO getComandaDAO() {
        return new CompandaImplMongoDB();
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpMongoDB();
    }

    @Override
    public ProducteDAO getProducteDAO() {
        return new ProducteImplMongoDB();
    }

    @Override
    public ProveidorDAO getProveidorDAO() {
        return new ProveidorImplMongoDB();
    }
}
