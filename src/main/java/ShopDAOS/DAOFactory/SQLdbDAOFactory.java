package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.comanda.CompandaImplSQL;
import ShopDAOS.DAO.empleat.EmpleatDAO;
import ShopDAOS.DAO.empleat.EmpleatImpSQL;
import ShopDAOS.DAO.producte.ProducteDAO;
import ShopDAOS.DAO.producte.ProducteImplSQL;
import ShopDAOS.DAO.proveidor.ProveidorDAO;
import ShopDAOS.DAO.proveidor.ProveidorImplSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLdbDAOFactory extends DAOFactory{
static Connection connection = null;
static String DRIVER = "org.postgresql.Driver";
static String URLDB = "jdbc:postgresql://tyke.db.elephantsql.com:5432/wbtcnqtq";
static String USUARIO = "wbtcnqtq";
static String CLAVE = "DOTrndAZXQXzSywvhR5Yhs7LG96tXvHt";

public SQLdbDAOFactory(){
    DRIVER = "org.postgresql.Driver";
    URLDB = "jdbc:postgresql://tyke.db.elephantsql.com:5432/wbtcnqtq";
}

//Crear conexion
    public static Connection crearConexion(){
        if(connection == null){
            try{
                Class.forName(DRIVER);
            }catch (ClassNotFoundException ex){
                Logger.getLogger(SQLdbDAOFactory.class.getName()).log(Level.SEVERE,null,ex);
            }
            try{
                connection = DriverManager.getConnection(URLDB,USUARIO,CLAVE);
            }catch (SQLException e){
                System.out.println("Ha ocurrido una excepción en SQLdbDAOFactory");
                System.err.println(e.getMessage());
            }
        }
        return connection;
    }

    @Override
    public ComandaDAO getComandaDAO() {
        return new CompandaImplSQL();
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpSQL();
    }

    @Override
    public ProducteDAO getProducteDAO() {
        return new ProducteImplSQL();
    }

    @Override
    public ProveidorDAO getProveidorDAO() {
        return new ProveidorImplSQL();
    }
}
